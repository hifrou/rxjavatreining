package pzet.pl.rxapplication;

import com.jakewharton.rxrelay2.BehaviorRelay;

import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.AsyncSubject;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;

import static org.junit.Assert.*;

public class RxJavaTest {

    private String getThreadName() {
        return Thread.currentThread().getName();
    }

    private <T> void onNext(T value) {
        System.out.println("Value: " + value + " Thread: " + getThreadName());
    }

    private <T> void onError(T exception) {
        System.out.println("Error: " + exception.getClass().getSimpleName() + " Thread: " + getThreadName());
    }

    private void onCompleted() {
        System.out.println("Completed. " + " Thread: " + getThreadName());
    }

    private void waitBeforeDispose(Disposable disposable, int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        disposable.dispose();
        System.out.println("Disposed");
    }

    Subscriber printinSubscriber = new Subscriber() {
        @Override
        public void onSubscribe(Subscription s) {

        }

        @Override
        public void onNext(Object o) {

        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onComplete() {

        }
    };

    //TODO: zapytać o subskryber w swifcie na przerwie

    @Test
    public void shoudTestJustOperator() {
        Observable<String> observable = Observable.just("Przemek", "Damian", "Kamil");
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
    }

    @Test
    public void fromArray() {
        Observable<String> observable = Observable.fromArray("Przemek", "Damian", "Kamil");
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void fromCallable() {
        Observable<Integer> observable = Observable.fromCallable(new Sum(1, 2));
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void fromRange() {
        Observable<Integer> observable = Observable.range(1, 10);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void emptyOperator() {
        Observable<Integer> observable = Observable.empty();
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void never() {
        Observable<Integer> observable = Observable.never();
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void createOperator() {
        Observable<Integer> observable = Observable.create(emitter -> {
            for (int i = 0; i < 10; i++) {
                System.out.println("Generating value, Thread: " + Thread.currentThread().getName());
                emitter.onNext(i);
                Thread.sleep(1000);
            }
            emitter.onComplete();
        });
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void deferOperator() {
        Observable<Integer> observableFactory = Observable.defer(() -> {
            Random random = new Random();
            return Observable.just(random.nextInt());
        });

        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(observableFactory.subscribe(this::onNext, this::onError, this::onCompleted));
        compositeDisposable.add(observableFactory.subscribe(this::onNext, this::onError, this::onCompleted));
        compositeDisposable.dispose();
    }

    @Test
    public void singleObservable() {
        Single single = Single.just("Piotrek");

        Disposable disposable = single.subscribe(this::onNext, this::onError);
        disposable.dispose();
    }

    @Test
    public void singleToObservable() {
        Single single = Single.just("Piotrek");

        Disposable disposable = single.toObservable().subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void completableObservable() {
        Completable completable = Completable.complete();

        Disposable disposable = completable.subscribe(this::onCompleted, this::onError);
        disposable.dispose();
    }

    @Test
    public void maybeObservable() {
        Maybe maybe = Maybe.just("Łukasz");

        Disposable disposable = maybe.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void publicSubject() {
        PublishSubject<Integer> publishSubject = PublishSubject.create();

        publishSubject.onNext(1);
        //I should see only items emitted after subscription
        Disposable disposable = publishSubject.subscribe(this::onNext, this::onError, this::onCompleted);
        publishSubject.onNext(6);
        publishSubject.onNext(2);

        disposable.dispose();
    }

    @Test
    public void behaviourSubject() {
        BehaviorSubject behaviorSubject = BehaviorSubject.create();

        behaviorSubject.onNext(1);//this value will be consumed in subscriber
        Disposable disposable = behaviorSubject.subscribe(this::onNext, this::onError, this::onCompleted);
        behaviorSubject.onNext(6);
        behaviorSubject.onNext(2);

        disposable.dispose();
    }

    @Test
    public void behaviourSubject2() {
        BehaviorSubject behaviorSubject = BehaviorSubject.createDefault(0); //initial value ignored, because overriden by a succeeding value

        behaviorSubject.onNext(1); //this value will be seen in subscriber
        Disposable disposable = behaviorSubject.subscribe(this::onNext, this::onError, this::onCompleted);
        behaviorSubject.onNext(6);
        behaviorSubject.onNext(2);

        disposable.dispose();
    }

    @Test
    public void behaviourSubject3() {
        BehaviorSubject behaviorSubject = BehaviorSubject.createDefault(0); //now the initial value is visible

        Disposable disposable = behaviorSubject.subscribe(this::onNext, this::onError, this::onCompleted);
        behaviorSubject.onNext(6);
        behaviorSubject.onNext(2);

        disposable.dispose();
    }

    @Test
    public void replaySubject() {
        ReplaySubject replaySubject = ReplaySubject.createWithSize(3); //this sets the size of buffer

        replaySubject.onNext(1);
        replaySubject.onNext(2);
        replaySubject.onNext(3);
        replaySubject.onNext(4);


        Disposable disposable = replaySubject.subscribe(this::onNext, this::onError, this::onCompleted);
        replaySubject.onNext(6);
        disposable.dispose();
    }

    @Test
    public void asyncSubject() {
        AsyncSubject asyncSubject = AsyncSubject.create(); //this sets the size of buffer

        asyncSubject.onNext(1);
        asyncSubject.onNext(2);
        asyncSubject.onNext(3);
        asyncSubject.onNext(4);


        Disposable disposable = asyncSubject.subscribe(this::onNext, this::onError, this::onCompleted);
//        asyncSubject.onNext(6);
        asyncSubject.onComplete();
        disposable.dispose();
    }

    @Test
    public void behaviourRelay() {
        BehaviorRelay behaviorRelay = BehaviorRelay.createDefault("123");

        Disposable disposable = behaviorRelay.subscribe(this::onNext, this::onError, this::onCompleted);
        behaviorRelay.accept(6);
        behaviorRelay.accept(2);

        disposable.dispose();
    }

    @Test
    public void ignoreElements() {
        Completable completable = Observable.range(1, 5).ignoreElements();
        Disposable disposable = completable.subscribe(this::onCompleted, this::onError);
        disposable.dispose();
    }

    @Test
    public void elementAtOperator() {
        Maybe maybe = Observable.range(1, 10).elementAt(0);
        Disposable disposable = maybe.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();

        Single<Integer> single = Observable.range(1, 10).elementAt(50, 30);
        Disposable disposable2 = single.subscribe(this::onNext, this::onError);
        disposable.dispose();
    }

    @Test
    public void filterOperator() {
        Observable<Integer> observable = Observable.range(1, 10).filter(value -> value % 3 == 0);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void skipOperator() {
        Observable<Integer> observable = Observable.range(1, 10).skip(5);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void skipWileOperator() {
        Observable<Integer> observable = Observable.range(1, 10).skipWhile(value -> value % 3 != 0);
        //this skips while predicate is true, once it is true for firs value, it lets all succeding items through, so everything after 3 is passed
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void skipUntilOperator() {
        PublishSubject trigger = PublishSubject.create();
        PublishSubject publishSubject = PublishSubject.create();

        Disposable disposable = publishSubject.skipUntil(trigger).subscribe(this::onNext, this::onError, this::onCompleted);

        publishSubject.onNext(1);
        publishSubject.onNext(2);
        publishSubject.onNext(3);
        trigger.onNext("triggered");
        publishSubject.onNext(4);
        publishSubject.onNext(5);
        publishSubject.onNext(6);

        disposable.dispose();
    }

    @Test
    public void takeUntilOperator() {
        PublishSubject trigger = PublishSubject.create();
        PublishSubject publishSubject = PublishSubject.create();

        Disposable disposable = publishSubject.takeUntil(trigger).subscribe(this::onNext, this::onError, this::onCompleted);

        publishSubject.onNext(1);
        publishSubject.onNext(2);
        publishSubject.onNext(3);
        trigger.onNext("triggered");
        publishSubject.onNext(4);
        publishSubject.onNext(5);
        publishSubject.onNext(6);

        disposable.dispose();
    }

    @Test
    public void distinctOperator() {
        Observable observable = Observable.just(1, 1, 2, 2, 3, 4, 4, 3, 3).distinct();
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void distinctUntilChangedOperator() {
        Observable observable = Observable.just(1, 1, 2, 2, 3, 4, 4, 3, 3).distinctUntilChanged();
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void mapOperator() {
        Observable<Integer> observable = Observable.range(1, 10).map(value -> value * 2);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void flatMapOperator() {
        Observable<Integer> observable = Observable.range(1, 2);
        Observable<Integer> observable2 = Observable.range(3, 2);
        Observable observableOfObservables = Observable.just(observable, observable2);

        Disposable disposable = observableOfObservables.flatMap(value -> value).subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void flatMapOperator2() {
        Observable<Integer> observable = Observable.just(1, 2, 3);
        Observable<Integer> observable2 = Observable.just(4, 5, 6);
        Observable observableOfObservables = Observable.just(observable, observable2);

        Disposable disposable = observableOfObservables.flatMap(value -> value).subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void switchMapOperator2() {
        Observable<Integer> observable = Observable.just(1, 2, 3);
        Observable<Double> observable2 = Observable.just(4.0, 5.1, 6.2);
        Observable<Object> observableOfObservables = Observable.just(observable, observable2);

        //TODO: play with it, this has a problem, does not return values, but observables instaead

        Disposable disposable = observableOfObservables.switchMap(val ->
                Observable.just(val.toString())
        )
                .subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void startWith() {
        Observable<Integer> observable = Observable.range(1, 10).startWith(3);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void concatWithOperator2() {
        Observable<Integer> observable = Observable.just(1, 2, 3);
        Observable<Integer> observable2 = Observable.just(7, 8, 9);
        Observable<Integer> observable3 = Observable.concat(observable, observable2);

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);
        disposable.dispose();
    }

    @Test
    public void concatMapOperator() {
        PublishSubject<Integer> ps1 = PublishSubject.create();
        PublishSubject<Integer> ps2 = PublishSubject.create();
        Observable<Integer> observable3 = Observable.just(ps1, ps2).concatMap(value -> value);

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);

        ps1.onNext(1);
        ps1.onNext(2);
        ps1.onNext(3);
        ps2.onNext(1);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void mergeOperator() {
        PublishSubject<Integer> ps1 = PublishSubject.create();
        PublishSubject<Integer> ps2 = PublishSubject.create();
        Observable<Integer> observable3 = Observable.merge(ps1, ps2);

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);

        ps1.onNext(1);
        ps1.onNext(2);
        ps1.onNext(3);
        ps2.onNext(1);
        ps2.onNext(123456);
        ps2.onNext(7);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void combineLatest() {
        PublishSubject<Integer> publishSubject = PublishSubject.create();
        PublishSubject<Integer> publishSubject2 = PublishSubject.create();
        Observable<String> observable3 = Observable.combineLatest(publishSubject, publishSubject2, (integer, integer1) -> { return "" + integer.toString() + integer1.toString(); });

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);

        publishSubject.onNext(1);
        publishSubject.onNext(2);
        publishSubject.onNext(3);
        publishSubject2.onNext(1);
        publishSubject2.onNext(123456);
        publishSubject2.onNext(7);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void zipOperator() {
        PublishSubject<Integer> publishSubject = PublishSubject.create();
        PublishSubject<Integer> publishSubject2 = PublishSubject.create();
        Observable<String> observable3 = Observable.zip(publishSubject, publishSubject2, (integer, integer1) -> { return "" + integer.toString() + integer1.toString(); });

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);

        publishSubject.onNext(1);
        publishSubject.onNext(2);
        publishSubject.onNext(3);

        publishSubject2.onNext(1);
        publishSubject2.onNext(123456);
        publishSubject2.onNext(7);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void withLatestFromOperator() {
        PublishSubject<Integer> source = PublishSubject.create();
        PublishSubject<Integer> trigger = PublishSubject.create();

        Observable<String> observable3 = trigger.withLatestFrom(source, (triggerValue, sourceValue) -> triggerValue + ":" +sourceValue);

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);

        source.onNext(1);
        source.onNext(2);
        source.onNext(3);

        trigger.onNext(1);
        trigger.onNext(123456);
        trigger.onNext(7);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void withLatestFromOperator2() {
        PublishSubject<Integer> source = PublishSubject.create();
        PublishSubject<Integer> trigger = PublishSubject.create();

        Observable<String> observable3 = trigger.withLatestFrom(source, (triggerValue, sourceValue) -> triggerValue + ":" +sourceValue);

        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);

        source.onNext(1);

        trigger.onNext(1);
        trigger.onNext(123456);
        trigger.onNext(7);

        source.onNext(2);
        trigger.onNext(4);
        trigger.onNext(4);

        trigger.onNext(4);

        source.onNext(9);
        source.onNext(9);
        source.onNext(9);

        waitBeforeDispose(disposable, 2);
    }


    @Test
    public void sampleOperator2() {
        PublishSubject<Integer> source = PublishSubject.create();
        PublishSubject<Integer> trigger = PublishSubject.create();
        Observable<Integer> observable3 = source.sample(trigger);
        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);
        source.onNext(1);
        trigger.onNext(1);
        trigger.onNext(123456);
        trigger.onNext(7);

        source.onNext(2);
        trigger.onNext(4);
        trigger.onNext(4);
        trigger.onNext(4);

        source.onNext(9);
        source.onNext(9);
        source.onNext(9);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void ambOperator2() {
        PublishSubject<Integer> first = PublishSubject.create();
        PublishSubject<Integer> second = PublishSubject.create();
        PublishSubject<Integer> third = PublishSubject.create();
        Observable<Integer> observable3 = first.ambWith(second).ambWith(third);
        Disposable disposable = observable3.subscribe(this::onNext, this::onError, this::onCompleted);
        first.onNext(1);
        first.onNext(2);
        second.onNext(123456);
        second.onNext(7);
        third.onNext(2);
        third.onNext(3);
        third.onNext(5);
        first.onNext(4);
        second.onNext(9);
        second.onNext(9);
        second.onNext(9);
        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void reduce() {
        Single<Integer> observable = Observable.range(10, 5).reduce(0, Integer::sum);
        Disposable disposable = observable.subscribe(this::onNext, this::onError);

        waitBeforeDispose(disposable, 2);
    }

    @Test
    public void replay() {
        ConnectableObservable<Integer> observable = Observable.range(1, 10).replay(5);
        Disposable firstDisposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        observable.connect();
        Disposable secondDisposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);

        firstDisposable.dispose();
        secondDisposable.dispose();
    }

    @Test
    public void connectableObservable() {
        ConnectableObservable<Integer> observable = Observable.range(1, 10).publish(); //publish creates connectable observable from ordinary one
        Disposable firstDisposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        observable.connect();
        Disposable secondDisposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);

        firstDisposable.dispose();
        secondDisposable.dispose();
    }

    @Test
    public void buffer() {
        Observable<List<Integer>> observable = Observable.range(1, 10).buffer(3);
        Disposable firstDisposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        firstDisposable.dispose();
    }

    @Test
    public void intervalOperator() {
        Observable<Long> observable = Observable.interval(1, TimeUnit.SECONDS);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        waitBeforeDispose(disposable, 10);
    }

    @Test
    public void timerOperator() {
        Observable<Long> observable = Observable.timer(3, TimeUnit.SECONDS);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        waitBeforeDispose(disposable, 4);
    }

    @Test
    public void delayOperator() { //this delays start of whole sequence, not between consecutive emissions
        Observable<Long> observable = Observable.interval(1, TimeUnit.SECONDS).delay(2, TimeUnit.SECONDS);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        waitBeforeDispose(disposable, 10);
    }

    @Test
    public void timeoutOperator() { //this delays start of whole sequence, not between consecutive emissions
        Observable<Long> observable = Observable.timer(2, TimeUnit.SECONDS).timeout(500, TimeUnit.MILLISECONDS);
        Disposable disposable = observable.subscribe(this::onNext, this::onError, this::onCompleted);
        waitBeforeDispose(disposable, 10);
    }

    @Test
    public void schedulersSubscribeOn() {
        Observable<Integer> observable = Observable.create(emitter -> {
            for (int i = 0; i < 10; i++) {
                System.out.println("Generating value, Thread: " + Thread.currentThread().getName());
                emitter.onNext(i);
                Thread.sleep(1000);
            }
            emitter.onComplete();
        });
        Disposable disposable = observable
                .subscribeOn(Schedulers.io())
                .subscribe(this::onNext, this::onError, this::onCompleted);
        waitBeforeDispose(disposable, 10);
    }

    @Test
    public void schedulersSubscribeOnWithObserveOn() {
        Observable<Integer> observable = Observable.create(emitter -> {
            for (int i = 0; i < 10; i++) {
                System.out.println("Generating value, Thread: " + Thread.currentThread().getName());
                emitter.onNext(i);
                Thread.sleep(1000);
            }
            emitter.onComplete();
        });
        Disposable disposable = observable
                .subscribeOn(Schedulers.io())
                .doOnNext(value -> getThreadName())
                .observeOn(Schedulers.newThread())
                .subscribe(this::onNext, this::onError, this::onCompleted);
        waitBeforeDispose(disposable, 10);

    }


}
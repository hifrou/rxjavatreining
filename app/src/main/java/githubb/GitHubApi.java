package githubb;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubApi {

    @GET("users/{user}/repos")
    public Observable<List<Repository>> getRepositoryByUser(@Path("username") String username);

    @GET("search/repositories")
    public Observable<QueryResult> getRepositoriesByQuery(@Query("q") String query);
}

package pzet.pl.rxapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.jakewharton.rxbinding3.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import githubb.GitHubApi;
import githubb.QueryResult;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MainActivity extends AppCompatActivity {

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    private OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build();

    private Retrofit retrofitBuilder(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    private final GitHubApi githubApi = retrofitBuilder("https://api.github.com").create(GitHubApi.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText query = findViewById(R.id.editText);

        compositeDisposable.add(RxTextView.textChanges(query)
                .map(CharSequence::toString)
                .filter(text -> !text.isEmpty())
                .debounce(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onQuery, this::onError));
    }

    private void onQuery(String query) {
        setTitle(query);
        compositeDisposable.add(githubApi.getRepositoriesByQuery(query)
                .subscribeOn(Schedulers.io())
                .subscribe(this::onNext, this::onError, this::onCompleted));
    }

    private <T> void onNext(T value) {
        System.out.println("Value: " + value + " Thread: " + String.valueOf(value));
    }

    private String getThreadName() {
        return Thread.currentThread().getName();
    }

    private <T> void onError(T exception) {
        System.out.println("Error: " + exception.getClass().getSimpleName() + " Thread: " + getThreadName());
    }

    private void onCompleted() {
        System.out.println("Completed. " + " Thread: " + getThreadName());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    private void onError(Throwable error){
        Log.e(MainActivity.class.getSimpleName(), String.valueOf(error.getMessage()));
    }
}
